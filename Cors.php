<?php
namespace Iss\Api\Service\Cors;

use Iss\Api\Service\Core\Representation\Formatter\Blank;
use Iss\Api\Service\Core\Representation\Representation;
use Iss\Api\{ServiceInterface, ServiceTrait};
use Phalcon\Mvc\Micro;
use Phalcon\Http\{Response, Request};
use Phalcon\Events\Event;

class Cors implements ServiceInterface
{
    use ServiceTrait;

    public function register(Micro $application): bool
    {
        if (!$application->getDI()->has('representation')) {
            // representation service is not ready yet
            return false;
        }

        /**
         * @var $representation Representation
         */
        $representation = $application['representation'];
        $this->setPriorityAfter($representation);

        $application->eventsManager->attach('micro:beforeExecuteRoute', $this, $this->getPriority());
        $application->eventsManager->attach('micro:beforeHandleRoute', $this, $this->getPriority());
        $application->eventsManager->attach('api:beforeHandleRequest', $this, $this->getPriority());
        $application->setService(self::getName(), $this, true);
        return true;
    }

    public function unregister(Micro $application): ?ServiceInterface
    {
        $application->getEventsManager()->detach('micro:beforeExecuteRoute', $this);
        $application->getEventsManager()->detach('micro:beforeHandleRoute', $this);
        $application->getEventsManager()->detach('api:beforeHandleRequest', $this);
        $application->getDI()->remove(self::getName());
        return $this;
    }

    public static function getName(): string
    {
        return 'cors';
    }

    protected function setHeaders(Request $request, Response $response)
    {
        $headers = $request->getHeaders();
        if (isset($headers['Origin'])) {
            $response->setHeader('Access-Control-Allow-Origin', $headers['Origin']);
        }

        if ($request->isOptions()) { // CORS Preflight
            $response->setHeader('Access-Control-Max-Age', '1520000');

            if (isset($headers['Access-Control-Request-Headers'])) {
                $response->setHeader('Access-Control-Allow-Headers', $headers['Access-Control-Request-Headers']);
            }

            if (isset($headers['Access-Control-Request-Method'])) {
                $response->setHeader('Access-Control-Allow-Methods', $headers['Access-Control-Request-Method']);
            }
        }
    }

    public function beforeHandleRequest(Event $event, Micro $application)
    {
        $request = $application['request'];
        if ($request->isOptions()) {
            $application['route']->unregister($application);
        }
    }

    public function beforeHandleRoute(Event $event, Micro $application)
    {
        $request = $application['request'];
        $response = $application['response'];
        $this->setHeaders($request, $response);

        if ($request->isOptions()) { // CORS Preflight
            $application->getRouter()->clear();
            $logger = $application['logger'];
            $logger['service'] = self::getName();
            $logger->notice('[%service%] Changing not found handler [%uuid%]');
            $application->notFound(function(...$arguments){return null;});
            /**
             * @var $representation Representation
             */
            $representation = $application['representation'];
            $representation->setFormatter(new Blank($representation->getFormatter()));
        }
    }

    public function beforeExecuteRoute(Event $event, Micro $application)
    {
        $request = $application['request'];
        $response = $application['response'];
        $this->setHeaders($request, $response);

        if ($request->isOptions()) { // CORS Preflight
            $logger = $application['logger'];
            $logger['service'] = self::getName();
            $logger->notice('[%service%] Changing active handler [%uuid%]');
            $application->setActiveHandler(function(...$arguments){return null;});
            /**
             * @var $representation Representation
             */
            $representation = $application['representation'];
            $representation->setFormatter(new Blank($representation->getFormatter()));
        }
    }
}